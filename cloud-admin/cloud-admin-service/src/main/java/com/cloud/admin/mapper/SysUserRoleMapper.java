/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.admin.beans.dto.UserDTO;
import com.cloud.admin.beans.po.SysUserRole;

/**
 * 用户-角色
 *
 * @author Aijm
 * @date 2019-08-25 21:08:47
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    /**
     * 删除用户角色关联数据
     * @param userId
     * @return
     */
    int deleteUserRole(Long userId);

    /**
     * 根据角色删除用户和角色关系
     * @param roleId
     * @return
     */
    int deleteUserRoleByRole(Long roleId);


    /**
     * 插入用户角色关联数据
     * @param userDTO
     * @return
     */
    int insertUserRole(UserDTO userDTO);


}