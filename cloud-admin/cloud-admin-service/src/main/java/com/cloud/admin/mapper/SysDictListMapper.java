/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.mapper;

import com.cloud.admin.beans.po.SysDictList;
import com.cloud.common.data.base.ProMapper;

/**
 * 字典项list
 *
 * @author Aijm
 * @date 2019-09-05 19:52:37
 */
public interface SysDictListMapper extends ProMapper<SysDictList> {

}