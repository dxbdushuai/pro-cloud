/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.dto;

import com.cloud.admin.beans.po.SysDict;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author Aijm
 * @Description 字典类型表
 * @Date 2019/9/5
 */
@Data
@Accessors(chain = true)
public class DictDTO extends SysDict {

    /**
     * 字典结构类型  0：list 集合 1：树
     */
    public static final String DICT_TREE = "1";

    public static final String DICT_LIST = "0";

    /**
     * 字典类型  0：普通字典 1：系统级别数据 不允许删除
     */
    public static final Integer DICT_SYS = 1;

    public static final Integer DICT_NO_SYS = 0;
}