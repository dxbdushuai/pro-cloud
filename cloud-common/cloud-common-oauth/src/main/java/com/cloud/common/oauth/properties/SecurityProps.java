/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.oauth.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;


/**
 * @Author Aijm
 * @Description 这里是一个 security 属性
 * @Date 2019/5/19
 */
@Data
@ConfigurationProperties(prefix = "pro-cloud.security")
@Configuration
@RefreshScope
public class SecurityProps {


	/**
	 * 验证码配置
	 */
	private ValidateCodeProps code = new ValidateCodeProps();


	/**
	 * 验证码配置
	 */
	private ClientProps client = new ClientProps();


}