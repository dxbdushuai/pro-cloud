/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.security.component;


import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * UserDetails 配置
 * @author Aijm
 * @since 2019/6/9
 */
public class SecurityUser extends User {


	/**
	 * 存储用户的id 为了方便查询用户详细信息
	 */
	@Getter
	@Setter
	private Long userId;

	/**
	 * 用户类型
	 */
	@Getter
	@Setter
	private Integer userType;

	/**
	 * 能够管理的租户
	 */
	@Getter
	@Setter
	private Integer tenantId;


	public SecurityUser(String username, String password, Long userId,
						Integer userType, Integer tenantId) {
		super(username, password, AuthorityUtils.NO_AUTHORITIES);
		this.userId = userId;
		this.userType = userType;
		this.tenantId = tenantId;
	}

	public SecurityUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public SecurityUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

}