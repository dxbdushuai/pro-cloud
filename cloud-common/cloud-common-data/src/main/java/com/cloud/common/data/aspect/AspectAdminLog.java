/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.aspect;


import com.alibaba.fastjson.JSONObject;
import com.cloud.common.data.annotation.AdminLog;
import com.cloud.common.data.entity.SysLog;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;


/**
 * 系统日志，切面处理类
 * @author Aijm
 * @since 2019/10/9
 */
@Aspect
@AllArgsConstructor
@Slf4j
@Component
public class AspectAdminLog {


	@SneakyThrows
	@Around("@annotation(adminLog)")
	public Object saveSysLog(ProceedingJoinPoint point, AdminLog adminLog) {

		SysLog sysLog = new SysLog();
		if (adminLog != null) {
			// 注解上的描述
			sysLog.setOperation(adminLog.value());
		}
		Object obj = point.proceed();
		log.info("切面日志:{}", JSONObject.toJSONString(sysLog));
		return obj;
	}

}