/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.util.client;


import lombok.experimental.UtilityClass;

/**
 * 所有的feign
 * @author Aijm
 * @since 2019/5/4
 */
@UtilityClass
public class CloudServiceList {

    /**
     * 网关
     */
    public static final String CLOUD_GATEWAY = "cloud_gateway";

    /**
     * 用户模块
     */
    public static final String CLOUD_ADMIN = "cloud-admin-service";


    /**
     * 短信模块
     */
    public static final String CLOUD_MESSAGE = "cloud-message-service";


    /**
     * 认证模块
     */
    public static final String CLOUD_AUTH = "cloud-auth";
}