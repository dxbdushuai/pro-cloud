/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.business.controller;

import com.cloud.business.api.IOrderTbService;
import com.cloud.business.api.IStorageTbService;
import com.cloud.business.beans.po.OrderTb;
import com.cloud.business.beans.po.StorageTb;
import com.cloud.common.util.base.Result;

import io.seata.spring.annotation.GlobalTransactional;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 *
 *
 * @author Aijm
 * @date 2020-03-15 18:34:59
 */
@RestController
@RequestMapping("/ordertbAll" )
@Api(value = "ordertbAll", tags = "ordertbAll管理")
public class OrderTbAllController {

    @Autowired
    private IOrderTbService orderTbService;
    @Autowired
    private IStorageTbService storageTbService;



    /**
     * 新增
     * @param orderTb
     * @return Result
     */
    @PostMapping
    @GlobalTransactional
    public Result save() {
        OrderTb orderTb = new OrderTb();
//        Result save = orderTbService.save(orderTb);
        StorageTb storageTb = new StorageTb();
        storageTb.setId(1L);
        storageTb.setCount(1);
        Result result = storageTbService.updateById(storageTb);
        int i = 1/0;
        return Result.success("操作成功");
    }


}