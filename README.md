# Pro-Cloud
##    ----------------------- 如果喜欢的话，请给个star, 支持一下作者 -------------------------
**以下是Pro-Cloud架构简介**
### 介绍 
   Pro-Cloud 是一个Security作为安全框架,采用前后端分离的模式. 基于OAuth2 的RBAC权限管理微服务系统. Pro-Cloud后端采用springcloud alibaba架构,集成Sentinel从流量控制、熔断降级、Dubbo调用,系统负载等多个维度保护服务的稳定性。
注册中心、配置中心选型Nacos，为工程瘦身的同时加强各模块之间的联动。使用OAuth2，实现了多终端认证系统，可控制子系统的token权限互相隔离。
使用SkyWalking链路最终技术,方便了解应用拓扑和慢服务监测等情况,prometheus+grafana+alertManager作为监控中心,它为actuator端点提供了良好的交互界面，并提供了额外的特性.
mybatisplus的使用,大大的节省了开发的工作量,让代码更易维护 前端基于vue开发,降低学习成本,快速上手,
### [演示环境](http://119.23.8.73:1314/index.html)
服务器资源有限,监控模块没有部署 敬请谅解: [演示地址](http://119.23.8.73:1314/index.html)
### Pro-Cloud架构图
![输入图片说明](./docs/image/7.png "spring cloud 微服务.png")
### 项目地址
[gitee项目地址链接](https://gitee.com/gitsc/pro-cloud)  
### 相关工程
后台管理前端vue工程（pro-ui 待支持）：[码云地址](https://gitee.com/gitsc/pro-ui)  

### 文档
详细请参考: [pro-cloud技术文档](https://www.kancloud.cn/aijm/pro-cloud/1340604)


### 软件架构
前台采用 vue.js 为核心框架;
后台基于 Spring Cloud alibaba2.2.1、Spring Security Oauth 2.0 开发企业级认证与授权，提供常见服务监控、链路追踪、日志分析、缓存管理、任务调度等实现，
nacos + Spring Cloud Oauth2 + Spring Cloud gateway +  Dubbo + mybatisplus等，各种组件注解开发，让代码简洁，通俗易通，以提高效率
```
Pro-Cloud
├── cloud-admin -- 系统基础模块
│   ├── cloud-admin-api   -- admin暴露的dubbo接口
│   └── cloud-admin-service -- admin模块的实现
├── cloud-auth  -- auth服务端 统一登录中心(支持单点登录和三方登录)
├─cloud-common   -- 系统公共模块
│  ├─cloud-common-cache  -- 缓存工具类+redis 分布式锁
│  ├─cloud-common-data  -- 对数据库操作工具类
│  ├─cloud-common-entity  -- 公共实体工具类
│  ├─cloud-common-job   -- 定时任务工具类
│  ├─cloud-common-mq    -- mq工具类
│  ├─cloud-common-oauth  -- oauth授权工具类
│  ├─cloud-common-oss    -- oss文件上传工具类
│  ├─cloud-common-security  -- 客户端安全工具类
│  ├─cloud-common-swagger -- swagger工具类
│  ├─cloud-common-util   -- 基础工具类
├─cloud-gateway   -- springcloud gateway 网关 
├─cloud-generator   -- 代码生成
├─cloud-monitor  -- 监控模块
├─cloud-oss  -- oss文件上传模块
│  ├─cloud-oss-api
│  └─cloud-oss-service
├─cloud-transaction   -- 分布式事务
├─cloud-xxl-job   -- xxl-job案例
└── docs    -- pro-cloud文档
```
   
| 版本规划| 解决问题|
|:----: |:--------:|
| v1.0 | elk日志收集文档完善 当前版本|

### 主要中间件区别
| pro-cloud 项目| pro-micro项目|
|:----: |:--------:|
| dubbo | dubbo |
| logback | log4j2|
| 不支持灰度发布 | 灰度发布|

 
### 如何参与代码贡献
详细参考:[代码贡献](./docs/如何贡献代码/如何贡献代码.md)

#### 参与贡献

1. [Mybatis-Plus](https://mp.baomidou.com/)
2. [Spring Cloud Oauth2](https://spring.io/projects/spring-security-oauth)
3. [Nacos](https://nacos.io/zh-cn/docs/quick-start.html)
4. [hutool](https://www.hutool.cn/docs/#/)

#### 欢迎加群
[![加入QQ群](https://img.shields.io/badge/238254944-blue.svg)](https://jq.qq.com/?_wv=1027&k=57NNidS)

![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/115300_6bd86d98_1236464.png "Pro-Cloud交流群群聊二维码.png")
#### Pro-Cloud建设

![输入图片说明](./docs/image/1.png "2.png")
![输入图片说明](./docs/image/2.png "屏幕截图.png")
![输入图片说明](./docs/image/3.png "屏幕截图.png")
![输入图片说明](./docs/image/4.png "屏幕截图.png")
![输入图片说明](./docs/image/5.png "屏幕截图.png")
![输入图片说明](./docs/image/6.png "屏幕截图.png")
