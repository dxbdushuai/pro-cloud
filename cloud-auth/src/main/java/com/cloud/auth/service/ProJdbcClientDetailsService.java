/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.auth.service;

import com.cloud.common.cache.annotation.Cache;
import com.cloud.common.cache.annotation.CachePut;
import com.cloud.common.cache.constants.CacheScope;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

/**
 * 实现mysql 管理数据库
 * @author Aijm
 * @since 2019/7/27
 */
@Service
public class ProJdbcClientDetailsService extends JdbcClientDetailsService {

    public ProJdbcClientDetailsService(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    @Cache( scope = CacheScope.AUTH_CLIENT,  key = "#clientId" , expire = 3600)
    public ClientDetails loadClientByClientId(String clientId) throws InvalidClientException {
        return super.loadClientByClientId(clientId);
    }
}