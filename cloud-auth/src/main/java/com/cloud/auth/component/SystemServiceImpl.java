/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.auth.component;

import com.cloud.auth.entity.SysTenant;
import com.cloud.auth.service.ProJdbcClientDetailsService;
import com.cloud.auth.service.SysTenantService;
import com.cloud.common.data.user.SystemService;
import com.cloud.common.data.util.ServletUtil;
import com.cloud.common.oauth.properties.SecurityProps;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Service;

/**
 * 默认用户信息
 * @author Aijm
 * @since 2019/8/25
 */
@Service
@AllArgsConstructor
public class SystemServiceImpl implements SystemService {

    private final SecurityProps securityProps;

    private final SysTenantService sysTenantService;

    /**
     * 获取到登录用户的id
     *
     * @return
     */
    @Override
    public Long getUserId() {
        return null;
    }

    /**
     * 获取到用户的租户id集合  默认值
     *
     * @return
     */
    @Override
    public Integer getUserTenantId() {
        // 从head 中获取到  clientId
        String clientId = ServletUtil.getClientId(securityProps.getClient().getClientId());
        // 获取到 对应的 租户信息
        SysTenant sysTenant = sysTenantService.getSysTenant(clientId);
        return sysTenant == null? null : sysTenant.getTenantId();
    }

    @Override
    public Integer getUserType() {
        return null;
    }

    /**
     * 获取到用户名称
     *
     * @return
     */
    @Override
    public String getUserName() {
        return null;
    }


}